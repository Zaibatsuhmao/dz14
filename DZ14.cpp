#include <iostream>

int main()
{
    std::string str1 = "Ivan";
    std::cout << str1 << "\n";
    std::cout << "Characters: " << str1.length() << "\n";

    
    std::cout << "First character: " << str1[0] << "\n";

    std::string subLast = str1.substr(str1.length()-1);
    std::cout << "Last character: " << subLast << "\n";
}